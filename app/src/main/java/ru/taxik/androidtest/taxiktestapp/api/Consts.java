package ru.taxik.androidtest.taxiktestapp.api;

/**
 * Created by DG on 28.10.2015.
 */
public interface Consts
{
    String CITIES                        = "cities";
    String CITY_ID                       = "city_id";
    String CITY_NAME                     = "city_name";
    String CITY_API_URL                  = "city_api_url";
    String CITY_DOMAIN                   = "city_domain";
    String CITY_MOBILE_SERVER            = "city_mobile_server";
    String CITY_DOC_URL                  = "city_doc_url";
    String CITY_LATITUDE                 = "city_latitude";
    String CITY_LONGITUDE                = "city_longitude";
    String CITY_SPN_LATITUDE             = "city_spn_latitude";
    String CITY_SPN_LONGITUDE            = "city_spn_longitude";
    String LAST_APP_ANDROID_VERSION      = "last_app_android_version";
    String ANDROID_DRIVER_APK_LINK       = "android_driver_apk_link";
    String INAPP_PAY_METHODS             = "inapp_pay_methods";
    String CHRONOPAY                     = "chronopay";
    String TRANSFERS                     = "transfers";
    String EXPERIMENTAL_ECONOM_PLUS      = "experimental_econom_plus";
    String EXPERIMENTAL_ECONOM_PLUS_TIME = "experimental_econom_plus_time";
    String REGISTRATION_PROMOCODE        = "registration_promocode";
}
