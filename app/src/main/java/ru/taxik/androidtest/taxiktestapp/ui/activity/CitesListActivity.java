package ru.taxik.androidtest.taxiktestapp.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import ru.taxik.androidtest.taxiktestapp.R;

/**
 * Created by DG on 28.10.2015.
 */
public class CitesListActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cities_list);

        setTitle(R.string.title_activity_cities_list);
    }
}
