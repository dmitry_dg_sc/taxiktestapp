package ru.taxik.androidtest.taxiktestapp.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ru.taxik.androidtest.taxiktestapp.R;
import ru.taxik.androidtest.taxiktestapp.api.model.City;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback
{
    public static final String PARAMS_EXTRA_KEY = "city";

    private GoogleMap mMap;
    private City      mCity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if(getIntent() != null)
        {
            mCity = (City) getIntent().getParcelableExtra(PARAMS_EXTRA_KEY);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        float zoomFactory = 15;

        if(mCity == null)
        {
            // отправим на бермуды, если открыли активити без города
            mCity = new City();
            mCity.setCityLatitude((float) 26.629167);
            mCity.setCityLongitude((float) -70.883611);
            mCity.setCityName("Bermuda Triangle");
            zoomFactory = 5;
        }

        mMap = googleMap;

        // Add a marker in M and move the camera
        LatLng m = new LatLng(mCity.getCityLatitude(), mCity.getCityLongitude());

        mMap.addMarker(new MarkerOptions().position(m).title(mCity.getCityName()));

        // сперва зум, потом координаты, иначе промахнётся
        mMap.moveCamera(CameraUpdateFactory.zoomTo(zoomFactory));
        // потом координаты, иначе промахнётся
        mMap.animateCamera(CameraUpdateFactory.newLatLng(m));
        //mMap.setMyLocationEnabled(true);
    }
}
