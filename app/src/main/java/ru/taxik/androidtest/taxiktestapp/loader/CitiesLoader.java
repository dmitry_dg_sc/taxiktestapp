package ru.taxik.androidtest.taxiktestapp.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.google.gson.Gson;

import java.util.List;

import ru.taxik.androidtest.taxiktestapp.R;
import ru.taxik.androidtest.taxiktestapp.api.model.Cities;
import ru.taxik.androidtest.taxiktestapp.api.model.City;
import ru.taxik.androidtest.taxiktestapp.helper.RequestHelper;

/**
 * Created by DG on 28.10.2015.
 */
public class CitiesLoader extends AsyncTaskLoader<List<City>>
{
//    static final String JSON = "{\"cities\":[{\"city_id\":1,\"city_name\":\"Москва\",\"city_api_url\":\"http:\\/\\/beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7777\",\"city_doc_url\":\"http:\\/\\/beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":55.755773,\"city_longitude\":37.617761,\"city_spn_latitude\":0.964953,\"city_spn_longitude\":2.757568,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"inapp_pay_methods\":[\"chronopay\"],\"transfers\":true,\"experimental_econom_plus\":5,\"experimental_econom_plus_time\":40,\"registration_promocode\":true},{\"city_id\":2,\"city_name\":\"Санкт-Петербург\",\"city_api_url\":\"http:\\/\\/piter.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"piter.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7788\",\"city_doc_url\":\"http:\\/\\/piter.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":59.993492,\"city_longitude\":30.289062,\"city_spn_latitude\":0.840364,\"city_spn_longitude\":3.013,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":3,\"city_name\":\"Воронеж\",\"city_api_url\":\"http:\\/\\/voronezh.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"voronezh.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7789\",\"city_doc_url\":\"http:\\/\\/voronezh.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":51.683415,\"city_longitude\":39.182638,\"city_spn_latitude\":0.130182,\"city_spn_longitude\":0.376625,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":4,\"city_name\":\"Ростов-на-Дону\",\"city_api_url\":\"http:\\/\\/rostov.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"rostov.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7790\",\"city_doc_url\":\"http:\\/\\/rostov.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":57.19223,\"city_longitude\":39.418317,\"city_spn_latitude\":0.056843,\"city_spn_longitude\":0.188313,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":5,\"city_name\":\"Уфа\",\"city_api_url\":\"http:\\/\\/ufa.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"ufa.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7791\",\"city_doc_url\":\"http:\\/\\/ufa.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":54.738412,\"city_longitude\":55.983461,\"city_spn_latitude\":0.559397,\"city_spn_longitude\":1.216736,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":9,\"city_name\":\"Шахты\",\"parent_city\":4,\"city_api_url\":\"http:\\/\\/rostov.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"rostov.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7790\",\"city_doc_url\":\"http:\\/\\/rostov.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":47.708485,\"city_longitude\":40.215958,\"city_spn_latitude\":0.097761,\"city_spn_longitude\":0.197258,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":10,\"city_name\":\"Рязань\",\"city_api_url\":\"http:\\/\\/ryazan.beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"ryazan.beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7795\",\"city_doc_url\":\"http:\\/\\/ryazan.beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":54.610108,\"city_longitude\":39.71308,\"city_spn_latitude\":0.199161,\"city_spn_longitude\":0.360526,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\",\"transfers\":false},{\"city_id\":107,\"city_name\":\"Нахабино (московская область)\",\"parent_city\":1,\"city_api_url\":\"http:\\/\\/beta.taxistock.ru\\/taxik\\/api\\/client\\/\",\"city_domain\":\"beta.taxistock.ru\",\"city_mobile_server\":\"protobuf.taxistock.ru:7777\",\"city_doc_url\":\"http:\\/\\/beta.taxistock.ru\\/taxik\\/api\\/doc\\/\",\"city_latitude\":55.846204,\"city_longitude\":37.168567,\"city_spn_latitude\":0.037595,\"city_spn_longitude\":0.043736,\"last_app_android_version\":7045,\"android_driver_apk_link\":\"http:\\/\\/www.taxik.ru\\/a\\/taxik.apk\"}]}\n";
    private List<City> mCities;

    public CitiesLoader(Context context)
    {
        super(context);
    }

    /**
     * This is where the bulk of our work is done.  This function is
     * called in a background thread and should generate a new set of
     * data to be published by the loader.
     */
    @Override
    public List<City> loadInBackground()
    {
        if(RequestHelper.isConnectedToInternet(getContext()))
        {
            try
            {
                // server_url хранится build.gradle для модуля
                String jsonString = RequestHelper.getJsonString(getContext().getResources().getString(R.string.server_url));

                Gson gson = new Gson();
                Cities cities = gson.fromJson(jsonString, Cities.class);
                mCities = cities.getCities();

            }
            catch(Exception e)
            {

            }
        }

        return mCities;
    }

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(List<City> cities)
    {
        if(isReset())
        {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if(cities != null)
            {
                onReleaseResources(cities);
            }
        }
        List<City> oldCities = cities;
        mCities = cities;

        if(isStarted())
        {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(cities);
        }

        // At this point we can release the resources associated with
        // 'oldCities' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if(oldCities != null)
        {
            onReleaseResources(oldCities);
        }
    }

    /**
     * Handles a request to start the Loader.
     */
    @Override
    protected void onStartLoading()
    {
        if(mCities != null)
        {
            // If we currently have a result available, deliver it
            // immediately.
            deliverResult(mCities);
        }

        // Has something interesting in the configuration changed since we
        // last built the app list?
        boolean configChange = false;// // TODO: 28.10.2015  mLastConfig.applyNewConfig(getContext().getResources());

        if(takeContentChanged() || mCities == null || configChange)
        {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    /**
     * Handles a request to stop the Loader.
     */
    @Override
    protected void onStopLoading()
    {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    /**
     * Handles a request to cancel a load.
     */
    @Override
    public void onCanceled(List<City> apps)
    {
        super.onCanceled(apps);

        // At this point we can release the resources associated with 'apps'
        // if needed.
        onReleaseResources(apps);
    }

    /**
     * Handles a request to completely reset the Loader.
     */
    @Override
    protected void onReset()
    {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        // At this point we can release the resources associated with 'apps'
        // if needed.
        if(mCities != null)
        {
            onReleaseResources(mCities);
            mCities = null;
        }
    }

    /**
     * Helper function to take care of releasing resources associated
     * with an actively loaded data set.
     */
    protected void onReleaseResources(List<City> apps)
    {
        // For a simple List<> there is nothing to do.  For something
        // like a Cursor, we would close it here.
    }
}
