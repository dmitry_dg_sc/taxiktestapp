package ru.taxik.androidtest.taxiktestapp.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/**
 * Created by DG on 28.10.2015.
 */
public class RequestHelper
{
    // используем okhttp с заделом на будущее, если будет работа с сетью помимо единичного запроса списка городов
    private static final OkHttpClient HTTP_CLIENT = new OkHttpClient();

    public static String getJsonString(String url) throws Exception
    {
        if(url == null || url.length() == 0)
        {
            throw new IllegalArgumentException();
        }

        Request request = new Request.Builder().url(url).build();

        Response response = HTTP_CLIENT.newCall(request).execute();
        return response.body().string();
    }

    public static boolean isConnectedToInternet(Context context)
    {
        NetworkInfo networkinfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        return networkinfo != null && networkinfo.isConnected() && networkinfo.isAvailable();
    }
}
