package ru.taxik.androidtest.taxiktestapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.taxik.androidtest.taxiktestapp.R;
import ru.taxik.androidtest.taxiktestapp.api.model.City;

/**
 * Created by DG on 28.10.2015.
 */
public class CityAdapter extends ArrayAdapter<City>
{
    public class ViewHolder
    {
        public TextView cityName;

        public ViewHolder(View view)
        {
            cityName = (TextView) view.findViewById(R.id.city_name_text);
        }
    }

    private final LayoutInflater mInflater;

    public CityAdapter(Context context)
    {
        super(context, R.layout.city_item);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(List<City> data)
    {
        clear();
        if(data != null)
        {
            for(City city : data)
            {
                add(city);
            }
            // api11 addAll(data);
        }
    }

    /**
     * Populate new items in the list.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewholder;

        if(convertView == null || !(convertView.getTag() instanceof ViewHolder))
        {
            convertView = mInflater.inflate(R.layout.city_item, parent, false);
            viewholder = new ViewHolder(convertView);

            convertView.setTag(viewholder);
        }
        else
        {
            viewholder = (ViewHolder) convertView.getTag();
        }

        City city = getItem(position);
        viewholder.cityName.setText(city.getCityName());

        return convertView;
    }
}
