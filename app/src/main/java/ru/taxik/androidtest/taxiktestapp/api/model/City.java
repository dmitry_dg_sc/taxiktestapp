package ru.taxik.androidtest.taxiktestapp.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ru.taxik.androidtest.taxiktestapp.api.Consts;

/**
 * Created by DG on 28.10.2015.
 */
public class City implements Parcelable
{
    @SerializedName(Consts.CITY_ID)
    int          cityId;
    @SerializedName(Consts.CITY_NAME)
    String       cityName;
    @SerializedName(Consts.CITY_API_URL)
    String       cityApiUrl;
    @SerializedName(Consts.CITY_DOMAIN)
    String       cityDomain;
    @SerializedName(Consts.CITY_MOBILE_SERVER)
    String       cityMobileServer;
    @SerializedName(Consts.CITY_DOC_URL)
    String       cityDocUrl;
    @SerializedName(Consts.CITY_LATITUDE)
    float        cityLatitude;
    @SerializedName(Consts.CITY_LONGITUDE)
    float        cityLongitude;
    @SerializedName(Consts.CITY_SPN_LATITUDE)
    float        citySpnLatitude;
    @SerializedName(Consts.CITY_SPN_LONGITUDE)
    float        citySpnLongitude;
    @SerializedName(Consts.LAST_APP_ANDROID_VERSION)
    int          lastAppAndroidVersion;
    @SerializedName(Consts.ANDROID_DRIVER_APK_LINK)
    String       androidDriverApkLink;
    @SerializedName(Consts.INAPP_PAY_METHODS)
    List<String> inappPayMethods;
    @SerializedName(Consts.TRANSFERS)
    boolean      transfers;
    @SerializedName(Consts.EXPERIMENTAL_ECONOM_PLUS)
    int          experimentalEconomPlus;
    @SerializedName(Consts.EXPERIMENTAL_ECONOM_PLUS_TIME)
    int          experimentalEconomPlusTime;
    @SerializedName(Consts.REGISTRATION_PROMOCODE)
    boolean      registrationPromocode;

    public City()
    {

    }

    public City(Parcel in)
    {
        cityId = in.readInt();
        cityName = in.readString();
        cityApiUrl = in.readString();
        cityDomain = in.readString();
        cityMobileServer = in.readString();
        cityDocUrl = in.readString();
        cityLatitude = in.readFloat();
        cityLongitude = in.readFloat();
        citySpnLatitude = in.readFloat();
        citySpnLongitude = in.readFloat();
        lastAppAndroidVersion = in.readInt();
        androidDriverApkLink = in.readString();
        inappPayMethods = new ArrayList<String>();
        in.readStringList(inappPayMethods);
        transfers = in.readByte() == 1;
        experimentalEconomPlus = in.readInt();
        experimentalEconomPlusTime = in.readInt();
        registrationPromocode = in.readByte() == 1;
    }

    public static final Creator<City> CREATOR = new Creator<City>()
    {
        @Override
        public City createFromParcel(Parcel in)
        {
            return new City(in);
        }

        @Override
        public City[] newArray(int size)
        {
            return new City[size];
        }
    };

    public int getCityId()
    {
        return cityId;
    }


    public String getCityName()
    {
        return cityName;
    }

    public String getCityApiUrl()
    {
        return cityApiUrl;
    }

    public String getCityDomain()
    {
        return cityDomain;
    }

    public String getCityMobileServer()
    {
        return cityMobileServer;
    }

    public String getCityDocUrl()
    {
        return cityDocUrl;
    }

    public float getCityLatitude()
    {
        return cityLatitude;
    }

    public float getCityLongitude()
    {
        return cityLongitude;
    }

    public float getCitySpnLatitude()
    {
        return citySpnLatitude;
    }

    public float getCitySpnLongitude()
    {
        return citySpnLongitude;
    }

    public int getLastAppAndroidVersion()
    {
        return lastAppAndroidVersion;
    }

    public String getAndroidDriverApkLink()
    {
        return androidDriverApkLink;
    }

    public List<String> getInappPayMethods()
    {
        return inappPayMethods;
    }

    public boolean isTransfers()
    {
        return transfers;
    }

    public int getExperimentalEconomPlus()
    {
        return experimentalEconomPlus;
    }

    public int getExperimentalEconomPlusTime()
    {
        return experimentalEconomPlusTime;
    }

    public boolean isRegistrationPromocode()
    {
        return registrationPromocode;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(cityId);
        dest.writeString(cityName);
        dest.writeString(cityApiUrl);
        dest.writeString(cityDomain);
        dest.writeString(cityMobileServer);
        dest.writeString(cityDocUrl);
        dest.writeFloat(cityLatitude);
        dest.writeFloat(cityLongitude);
        dest.writeFloat(citySpnLatitude);
        dest.writeFloat(citySpnLongitude);
        dest.writeInt(lastAppAndroidVersion);
        dest.writeString(androidDriverApkLink);
        dest.writeStringList(inappPayMethods);
        dest.writeByte((byte) (transfers ? 1 : 0));
        dest.writeInt(experimentalEconomPlus);
        dest.writeInt(experimentalEconomPlusTime);
        dest.writeByte((byte) (registrationPromocode ? 1 : 0));
    }

    public void setCityLatitude(float cityLatitude)
    {
        this.cityLatitude = cityLatitude;
    }

    public void setCityLongitude(float cityLongitude)
    {
        this.cityLongitude = cityLongitude;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }
}
