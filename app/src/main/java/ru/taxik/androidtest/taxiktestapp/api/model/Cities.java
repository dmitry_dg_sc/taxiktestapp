package ru.taxik.androidtest.taxiktestapp.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.taxik.androidtest.taxiktestapp.api.Consts;

/**
 * Created by DG on 28.10.2015.
 */
public class Cities
{
    @SerializedName(Consts.CITIES)
    List<City> cities;

    public List<City> getCities()
    {
        return cities;
    }
}
