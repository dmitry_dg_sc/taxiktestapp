package ru.taxik.androidtest.taxiktestapp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ru.taxik.androidtest.taxiktestapp.R;
import ru.taxik.androidtest.taxiktestapp.api.model.City;
import ru.taxik.androidtest.taxiktestapp.loader.CitiesLoader;
import ru.taxik.androidtest.taxiktestapp.ui.activity.MapsActivity;
import ru.taxik.androidtest.taxiktestapp.ui.adapter.CityAdapter;

/**
 * Created by DG on 28.10.2015.
 */
public class CitiesListActivityFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<List<City>>
{
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar        mProgressLaunch;
    private ListView           mListView;
    private CityAdapter        mAdapter;
    private TextView           mEmptyText;

    public CitiesListActivityFragment()
    {
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_general_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mEmptyText = (TextView) view.findViewById(R.id.empty_text);
        mEmptyText.setVisibility(View.GONE);

        mListView = (ListView) view.findViewById(R.id.list_view);
        mListView.setDividerHeight(getResources().getDimensionPixelSize(R.dimen.list_divider_height));
        mListView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i)
            {

            }

            /**
             * http://www.survivingwithandroid.com/2014/05/android-swiperefreshlayout-tutorial.html
             *
             * @param absListView
             * @param firstVisibleItem
             * @param visibleItemCount
             * @param totalItemCount
             */
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                if(firstVisibleItem == 0 && visibleItemCount > 0 && absListView.getChildAt(0).getTop() >= 0)
                {
                    mSwipeRefreshLayout.setEnabled(true);
                }
                else
                {
                    mSwipeRefreshLayout.setEnabled(false);
                }
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(CitiesListActivityFragment.this.getActivity(), MapsActivity.class);
                intent.putExtra(MapsActivity.PARAMS_EXTRA_KEY, (Parcelable) mAdapter.getItem(position));

                startActivity(intent);
            }
        });

        mProgressLaunch = (ProgressBar) view.findViewById(R.id.progress_launch);
        mProgressLaunch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);


        mAdapter = new CityAdapter(getActivity());
        mListView.setAdapter(mAdapter);
        mEmptyText.setVisibility(View.VISIBLE);

        load();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_cities_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_refresh)
        {
            onRefresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void load()
    {
        mProgressLaunch.setVisibility(View.VISIBLE);
        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onRefresh()
    {
        mProgressLaunch.setVisibility(View.VISIBLE);
        setData(null);

        getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public Loader<List<City>> onCreateLoader(int id, Bundle args)
    {
        // This is called when a new Loader needs to be created.  This
        // sample only has one Loader with no arguments, so it is simple.
        return new CitiesLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<City>> loader, List<City> data)
    {
        hideSwipeRefreshLayout();

        // Set the new data in the adapter.
        setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<City>> loader)
    {
        // Clear the data in the adapter.
        setData(null);
    }

    public void setData(List<City> data)
    {
        mAdapter.setData(data);

        if(data == null || data.isEmpty())
        {
            mEmptyText.setVisibility(View.VISIBLE);
        }
        else
        {
            mEmptyText.setVisibility(View.GONE);
        }

        mProgressLaunch.setVisibility(View.GONE);
    }

    private void hideSwipeRefreshLayout()
    {
        if(mSwipeRefreshLayout.isRefreshing())
        {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}